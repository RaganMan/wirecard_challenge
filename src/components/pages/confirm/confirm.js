import React, { Component } from 'react';
import { View, Switch, ImageBackground, Image, TextInput, TouchableOpacity, Modal, TouchableHighlight, Alert } from 'react-native';
import bgImage from '../../../assets/bg.jpg';
import { Col, Row, Grid } from "react-native-easy-grid";
import { styles } from './constyle';
import { Actions } from 'react-native-router-flux';
import { List, ListItem, Left, Body, Right, Text, Content, Button } from 'native-base';

const Confirm = (props) => {
    handleSuccess = () => {
        Alert.alert(
            'Success',
            'You are done',
            [
                { text: 'View Reciept', onPress: () => console.log('Ask me later pressed') },

                { text: 'Done', onPress: () => Actions.Dash() },
                {
                    text: 'View Email Reciept',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
            ],
            { cancelable: false },
        );
    };

    return (
        <ImageBackground source={bgImage} style={styles.backgroundContainer}>
            <List style={{ marginTop: 65 }}>
                <ListItem>
                    <Left>
                        <Text>From Account</Text>
                    </Left>
                    <Body>
                        <Text>
                            {props.from.acc_type}
                        </Text>
                        <Text note numberOfLines={1}>
                            {props.from.id}
                        </Text>
                    </Body>
                </ListItem>
                <ListItem>
                    <Left>
                        <Text>Transfer To</Text>
                    </Left>
                    <Body>
                        <Text>
                            {props.to.acc_type}
                        </Text>
                        <Text note numberOfLines={1}>
                            {props.to.id}
                        </Text>
                    </Body>
                </ListItem>
            </List>
            <List>
                <ListItem>
                    <Left>
                        <Text>When</Text>
                    </Left>
                    <Body>
                        <Text>
                            {`${props.date}/${props.month}/${props.year}`}
                        </Text>
                    </Body>
                </ListItem>
                <ListItem>
                    <Left>
                        <Text>Amount</Text>
                    </Left>
                    <Body>
                        <Text>
                            {props.amount}
                        </Text>
                    </Body>
                </ListItem>
                <ListItem>
                    <Left>
                        <Text>Recipient Referrence</Text>
                    </Left>
                    <Body>
                        <Text>
                            {props.recp}
                        </Text>

                    </Body>
                </ListItem>
            </List>
            <Content>
                <List>
                    <ListItem>
                        <Left>
                            <Button light onPress={() => Actions.pop()}> <Text> Cancel </Text></Button>
                        </Left>
                        <Body>
                            <Button success onPress={() => this.handleSuccess()}><Text> Success </Text></Button>
                        </Body>
                    </ListItem>
                </List>
            </Content>
        </ImageBackground>
    )
}

export default Confirm
