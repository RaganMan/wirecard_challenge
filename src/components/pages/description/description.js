import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Text, View, Switch, ImageBackground, Image,Alert, TextInput, TouchableOpacity, Modal, TouchableHighlight } from 'react-native';
import bgImage from '../../../assets/bg.jpg';
import { Col, Row, Grid } from "react-native-easy-grid";
import { styles } from './desStyle';
import Icon from 'react-native-vector-icons/Ionicons';
import From from '../ownAccountTransfer/from/from';
import To from '../ownAccountTransfer/To/to';
import { getDate } from '../../../store/actions/testAction';
import { Actions } from 'react-native-router-flux';
class Description extends Component {
    constructor(props) {
        super(props);
        this.state = {
            recRef: '',
            other: '',
            today: false,
            later: false
        }
    }
    componentWillReceiveProps(newProps) {
        console.log(newProps)
    }
    switchLaterToggle = () => {
        this.setState({
            later: !this.state.later
        })
    }
    switchTodayToggle = () => {
        this.setState({
            today: !this.state.today,
            later: false
        })
        this.props.getDate();
    }
    handleConfirm = () =>{
        if(!this.state.recRef){
            Alert('Please Insert Recepient')
        }
        else{
            Actions.Con({
                from:this.props.from,
                to:this.props.to,
                amount:this.props.amount,
                recp:this.state.recRef,
                date:this.props.date,
                month:this.props.month,
                year:this.props.year
            })
        }
    }
    render() {

        console.log(this.props)

        return (
            <ImageBackground source={bgImage} style={styles.backgroundContainer}>
                <Grid style={{ marginTop: 65 }}>
                    <Row size={30} style={styles.row40}>
                        <From render={true} data={this.props.from} />
                        <To render={true} data={this.props.to} />
                    </Row>
                    <Row size={15} style={styles.row10}>
                        <Grid>
                            <Row style={{ justifyContent: 'center', alignItems: 'center' }} size={5}>
                                <View>
                                    <Text style={{ fontSize: 23, color: 'white' }}>Amount</Text>
                                </View>
                            </Row>
                            <Row style={{ justifyContent: 'center', alignItems: 'center' }} size={5}>
                                <View>
                                    <Text style={{ fontSize: 40, color: 'yellow' }}>{this.props.amount}</Text>
                                </View>
                            </Row>
                        </Grid>
                    </Row>
                    <Row size={15} style={styles.row50}>
                        <Grid>
                            <Row style={{ justifyContent: 'center', alignItems: 'center' }} size={5}>
                                <TextInput
                                    style={styles.input}
                                    placeholder={'Recipient Reference'}
                                    placeholderTextColor={'rgba(255,255,255,0.7)'}
                                    underlineColorAndroid='transparent'
                                    onChangeText={(text) => { this.setState({ recRef: text }) }}
                                />
                            </Row>
                            <Row style={{ justifyContent: 'center', alignItems: 'center' }} size={5}>
                                <TextInput
                                    style={styles.input}
                                    placeholder={'Other Payment Details'}
                                    placeholderTextColor={'rgba(255,255,255,0.7)'}
                                    underlineColorAndroid='transparent'
                                    onChangeText={(text) => { this.setState({ other: text }) }}
                                />
                            </Row>
                        </Grid>


                    </Row>
                    <Row size={30} style={styles.row50}>
                        <Grid>
                            <Row size={0.5} style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ textAlign: 'center', color: 'white', fontSize: 16 }}>When</Text>
                            </Row>
                            <Row size={2} >
                                <Grid>
                                    <Row size={1} >
                                        <Grid>
                                            <Col style={{ justifyContent: 'flex-start' }}>
                                                <Text>Today</Text>
                                            </Col>
                                            <Col style={{ justifyContent: 'flex-end' }}>
                                                <Switch
                                                    onValueChange={this.switchTodayToggle}
                                                    value={this.state.today} />

                                            </Col>
                                        </Grid>
                                    </Row>
                                    <Row size={1} >
                                        <Col style={{ justifyContent: 'flex-start' }}>
                                            <Text>Later</Text>
                                        </Col>
                                        <Col style={{ justifyContent: 'flex-end' }}>
                                            <Switch
                                                onValueChange={this.switchLaterToggle}
                                                value={this.state.later} />
                                        </Col>
                                    </Row>
                                </Grid>
                            </Row>
                            <Row size={1} >
                                <Grid>
                                <Col style={{ justifyContent: 'center' }} >
                                    <TouchableOpacity style={styles.btnCan} onPress={()=>{Actions.pop()}}>
                                        <Text style={styles.text}>Cancel</Text>
                                    </TouchableOpacity>
                                </Col>
                                <Col style={{ justifyContent: 'center' }}>
                                    <TouchableOpacity style={styles.btnPro} onPress={()=>this.handleConfirm()}>
                                        <Text style={styles.text}>Proceed</Text>
                                    </TouchableOpacity>
                                </Col>
                                </Grid>
                            </Row>

                        </Grid>
                    </Row>
                </Grid>
            </ImageBackground>
        )
    }

}

const mapStateToProps = state => {
    return {
        date: state.test.date,
        dayoOfweek: state.test.dayOfWeek,
        month: state.test.month,
        time: state.test.time,
        year: state.test.year,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getDate: () => {
            dispatch(getDate())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Description);
